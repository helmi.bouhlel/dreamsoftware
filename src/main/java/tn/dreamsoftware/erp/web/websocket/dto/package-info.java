/**
 * Data Access Objects used by WebSocket services.
 */
package tn.dreamsoftware.erp.web.websocket.dto;
