/**
 * View Models used by Spring MVC REST controllers.
 */
package tn.dreamsoftware.erp.web.rest.vm;
